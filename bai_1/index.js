function tinhDiemKhuVuc(area){
    var diemKhuVuc;
    if(area =="A"){
        diemKhuVuc = 2;
    }else if(area=="B"){
        diemKhuVuc = 1;
    }else if(area=="C"){
        diemKhuVuc = 0.5;
    }else{
        diemKhuVuc = 0
    }
    return diemKhuVuc;
}
function tinhDiemDoiTuong(object){
    var diemDoiTuong;
    if(object=="1"){
        diemDoiTuong = 2.5;
    }else if(object=="2"){
        diemDoiTuong = 1.5;
    }else if(object=="3"){
        diemDoiTuong = 1;
    }else{
        diemDoiTuong = 0;
    }
    return diemDoiTuong;
}

// main
function ketQua(){
    var mon1 = document.getElementById("mon1").value*1;
    var mon2 = document.getElementById("mon2").value*1;
    var mon3 = document.getElementById("mon3").value*1;
    var diemTong = mon1 + mon2 + mon3;
    var khuVuc = document.getElementById("khuVuc").value;
    var doiTuong = document.getElementById("doiTuong").value;
    var diemTongket = diemTong + tinhDiemKhuVuc(khuVuc) + tinhDiemDoiTuong(doiTuong);
    var diemChuan = document.getElementById("diemChuan").value;
    if(mon1==0||mon2==0||mon3==0){
        document.getElementById("result").innerHTML = `
        <h4>có 1 môn bị điểm liệt => kết quả => rớt</h4>
        `
    }else if(diemTongket >= diemChuan){
        document.getElementById("result").innerHTML = `
        <h4>Điểm tổng kết của bạn là ${diemTongket}</h4>
        <h4>chúc mừng bạn đã đậu</h4>
     `
    }else{
        document.getElementById("result").innerHTML = `
        <h4>Điểm tổng kết của bạn là ${diemTongket}</h4>
        <h4>Xin lỗi bạn đã rớt</h4>
     `
    }
}